# CRUD (Create Read Update Delete) Web App using Grails Framework

Run this project by this command : `run-app`

### Screenshot

Home Page

![Home Page](img/home.png "Home Page")

Add Page

![Add Page](img/add.png "Add Page")

Details Page

![Details Page](img/details.png "Details Page")

List Page

![List Page](img/list.png "List Page")